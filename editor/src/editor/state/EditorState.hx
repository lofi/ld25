package editor.state;

import org.flixel.FlxObject;
import nme.Lib;
import nme.Assets;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.net.SharedObject;
import org.flixel.FlxButton;
import org.flixel.FlxG;
import org.flixel.FlxPath;
import org.flixel.FlxSave;
import org.flixel.FlxSprite;
import org.flixel.FlxState;
import org.flixel.FlxText;
import org.flixel.FlxU;

class EditorState extends FlxState {

	private var quit_label = "Quit";
	private var quit_button:FlxButton;
	private var load_label = "Load";
	private var load_button:FlxButton;
	private var save_label = "Save";
	private var save_button:FlxButton;

	var screen_center:Point;

	public function new() {
		super();
		screen_center = new Point(FlxG.width/2, FlxG.height/2);
	}

	override public function create():Void {
//		#if !neko
//		FlxG.bgColor = 0xff636363;
//		#else
//		FlxG.bgColor = {rgb: 0x131c1b, a: 0xff};
//		#end

		load_button = new FlxButton(10, 10, load_label, load);
		add(load_button);

		save_button = new FlxButton(20 + load_button.width, 10, save_label, save);
		add(save_button);

		quit_button = new FlxButton(FlxG.width - load_button.width - 10, 10, quit_label, quit);
		add(quit_button);

		FlxG.mouse.show();
	}

	override public function destroy():Void {
		super.destroy();
	}

	override public function update():Void {
		super.update();
	}

	override public function draw():Void {
		super.draw();
	}

	private function load():Void {}
	private function save():Void {}
	private function quit():Void {
		FlxG.switchState(new EditorMenuState());
	}
}
