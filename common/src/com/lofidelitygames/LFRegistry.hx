package com.lofidelitygames;

import nme.Assets;
import nme.media.Sound;
import nme.filesystem.File;
import sys.FileSystem;

import com.lofidelitygames.data.LFEntityFile;
import com.lofidelitygames.data.LFSceneFile;
import com.lofidelitygames.data.LFUtils;

class LFRegistry {

	static public var sfx:Hash<Sound>;
	static public var music:Hash<Sound>;

	static public var player:Dynamic;
	static public var president:Dynamic;
	static public var current_scene:Dynamic;

	static public var entity_descriptions:Hash<EntityDescription>;
	static public var scene_descriptions:Hash<SceneDescription>;

	static private function loadConfig(path_to_asset:String, loader:Dynamic):Dynamic {
		return loader.load(path_to_asset);
	}

	static public function init() {
		entity_descriptions = new Hash<EntityDescription>();
		entity_descriptions.set("test_player", loadConfig("assets/entities/test_player.xml", LFEntityFile));
		entity_descriptions.set("test_enemy", loadConfig("assets/entities/test_enemy.xml", LFEntityFile));
		entity_descriptions.set("insta_kill_trigger", loadConfig("assets/entities/insta_kill_trigger.xml", LFEntityFile));
		entity_descriptions.set("simple_trigger", loadConfig("assets/entities/simple_trigger.xml", LFEntityFile));
		entity_descriptions.set("president", loadConfig("assets/entities/president.xml", LFEntityFile));
		entity_descriptions.set("player", loadConfig("assets/entities/player.xml", LFEntityFile));
		entity_descriptions.set("idiot01", loadConfig("assets/entities/idiot01.xml", LFEntityFile));

		scene_descriptions = new Hash<SceneDescription>();
		scene_descriptions.set("test_scene", loadConfig("assets/scenes/test.xml", LFSceneFile));
		scene_descriptions.set("level01", loadConfig("assets/scenes/level01.xml", LFSceneFile));

		sfx = new Hash<Sound>();
		sfx.set("jump", Assets.getSound("assets/sfx/jump.wav"));
		sfx.set("hit", Assets.getSound("assets/sfx/hit.wav"));
		sfx.set("enemy_hit", Assets.getSound("assets/sfx/enemy_hit.wav"));
		sfx.set("charm", Assets.getSound("assets/sfx/charm.wav"));
		sfx.set("splode", Assets.getSound("assets/sfx/splode.wav"));
		sfx.set("throw", Assets.getSound("assets/sfx/throw.wav"));

		music = new Hash<Sound>();
		music.set("track01", Assets.getSound("assets/music/simple.wav"));
	}

	static public function reset() {
		entity_descriptions = null;
		scene_descriptions = null;
		sfx = null;

		init();
	}

}
