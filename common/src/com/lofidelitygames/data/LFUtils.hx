package com.lofidelitygames.data;

class LFUtils {

	static public function instanceFor(type:String, args:Array<Dynamic>):Dynamic {
		var resolved_class = Type.resolveClass(type);

		if (resolved_class == null) {
			throw "Class of type: " + type + ", does not exist!";
		}

		return Type.createInstance(resolved_class, args);
	}

}
