package editor.state;

import nme.Lib;
import nme.Assets;
import nme.geom.Rectangle;
import nme.net.SharedObject;
import org.flixel.FlxButton;
import org.flixel.FlxG;
import org.flixel.FlxPath;
import org.flixel.FlxSave;
import org.flixel.FlxSprite;
import org.flixel.FlxState;
import org.flixel.FlxText;
import org.flixel.FlxU;

class EditorMenuState extends FlxState {

	override public function create():Void {
		#if !neko
		FlxG.bgColor = 0xff636363;
		#else
		FlxG.bgColor = {rgb: 0x131c1b, a: 0xff};
		#end

		var scene_editor_button = new FlxButton(0, 0, "Scenes", function():Void {
			FlxG.switchState(new SceneEditorState());
		});
		add(scene_editor_button);
		var entity_editor_button = new FlxButton(0, 0, "Entities", function():Void {
			FlxG.switchState(new EntityEditorState());
		});
		add(entity_editor_button);
		var quit_button = new FlxButton(0, 0, "QUIT", function():Void {
			Lib.exit();
		});
		add(quit_button);

		var half_button_width = scene_editor_button.width / 2;
		var half_screen_width = FlxG.width / 2;
		var half_screen_height = FlxG.height / 2;
		var padding = 10;
		var half_padding = 5;

	  scene_editor_button.x = half_screen_width - scene_editor_button.width - half_padding;
		entity_editor_button.x = half_screen_width + half_padding;

		scene_editor_button.y = half_screen_height;
		entity_editor_button.y = half_screen_height;

		// quit button placement is bottom center
	  quit_button.x = half_screen_width - half_button_width;
	  quit_button.y = FlxG.height - quit_button.height - padding;

		FlxG.mouse.show();
	}
}
