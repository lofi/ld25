package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.LFRegistry;

class CoSimpleTrigger extends LFFlixelComponent {

	private var my_active_flag:Bool;
	private var my_target:LFFlixelEntity;

	override public function init(parameters:Hash<String>):Void {
		my_target = LFRegistry.player;
		my_active_flag = false;
	}

	override private function __update():Void {
		var overlapping = FlxG.overlap(my_entity, my_target);
		if (my_active_flag && !overlapping) {
			my_active_flag = false;
		} else if (!my_active_flag && overlapping) {
			my_active_flag = true;
			my_target.receive("trigger", "test trigger works!");
			FlxG.camera.shake(0.01, 0.1);
		}

	}
}
