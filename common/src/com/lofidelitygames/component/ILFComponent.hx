package com.lofidelitygames.component;

import com.lofidelitygames.entity.ILFEntity;

interface ILFComponent {

	private var my_mute:Bool;

	function init(parameters:Hash<String>):Void;
	function update():Void;
	function mute(state:Bool):Void;

}
