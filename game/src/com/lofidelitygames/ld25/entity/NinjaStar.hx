package com.lofidelitygames.ld25.entity;

import org.flixel.FlxG;
import org.flixel.FlxSprite;
import org.flixel.FlxObject;

class NinjaStar extends FlxSprite {
	public var damage:Float = 0.5;
	public var speed:Int = 300;

	private var my_timer:Float;
	private var my_duration:Float = 1;

	public function new() {
		super(0, 0, "assets/ninjastar.png");

		exists = false;
	}

	public function fire(sx:Float, sy:Float, direction:Int):Void {
		x = sx;
		y = sy;
		velocity.x = (direction == FlxObject.LEFT) ? -speed : speed;
		exists = true;

		my_timer = 0;
	}

	override public function update():Void {
		super.update();
		my_timer += FlxG.elapsed;
		if (velocity.x == 0 || my_timer > my_duration) {
			exists = false;
		}
	}
}
