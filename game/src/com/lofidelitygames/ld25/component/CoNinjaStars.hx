package com.lofidelitygames.ld25.component;

import org.flixel.FlxG;
import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.ld25.GameRegistry;
import com.lofidelitygames.component.LFFlixelComponent;

class CoNinjaStars extends LFFlixelComponent {
	override private function __update():Void {
		if (FlxG.keys.justReleased("X")) {
			GameRegistry.ninjastars.fire(
				my_entity.x + (my_entity.width/2),
				my_entity.y + (my_entity.height/2),
				my_entity.sprite.facing
			);
		}
	}
}
