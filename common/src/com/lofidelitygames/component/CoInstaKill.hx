package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.LFRegistry;

class CoInstaKill extends LFFlixelComponent {

	override public function init(parameters:Hash<String>):Void {
		// pass
	}

	override private function __update():Void {
		if (FlxG.overlap(my_entity, LFRegistry.player)) {
			LFRegistry.player.kill();
		}
	}
}
