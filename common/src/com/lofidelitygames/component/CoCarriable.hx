package com.lofidelitygames.component;
import org.flixel.FlxG;
import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.component.LFFlixelComponent;
import com.lofidelitygames.LFRegistry;

class CoCarriable extends LFFlixelComponent {
    private var carrier:LFFlixelEntity = null;

    override public function new(entity:LFFlixelEntity):Void {
        super(entity);
        entity.carriable = this;
    }

    override private function __update():Void {
        /*
        The idea here is to have this entity follow along with the player
        when being carried, but also facing the player when not being carried.
         */

        if (carrier != null) {
            my_entity.x = carrier.x;
            my_entity.y = carrier.y - 5; // TODO: make the offset configurable
        }
    }

    public function pickup(carrier:LFFlixelEntity):Void {
        this.carrier = carrier;
        FlxG.log({carrier: carrier});
    }

    public function drop():Void {
        carrier = null;
    }

    public function carried():Bool {
        return carrier != null;
    }

}
