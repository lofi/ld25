package com.lofidelitygames.component;

import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.component.ILFComponent;

class LFFlixelComponent implements ILFComponent {

	private var my_mute:Bool = false;
	private var my_entity:LFFlixelEntity;

	public function new(entity:LFFlixelEntity) {
		my_entity = entity;
	}

	public function init(parameters:Hash<String>):Void {
		// pass
	}

	public function update():Void {
		if (my_mute) {
			return;
		}
		__update();
	}

	private function __update():Void {}

	public function mute(state:Bool):Void {
		my_mute = state;
	}
}
