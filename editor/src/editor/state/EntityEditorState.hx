package editor.state;

import org.flixel.FlxG;

import com.lofidelitygames.component.ILFComponent;

import com.lofidelitygames.entity.ILFEntity;
import com.lofidelitygames.entity.LFFlixelEntity;

import com.lofidelitygames.data.LFSceneFile;
import com.lofidelitygames.data.LFEntityFile;

class EntityEditorState extends EditorState {
	public function new() {
		super();
	}

	override public function create():Void {
		super.create();

		#if !neko
		FlxG.bgColor = 0xff636300;
		#else
		FlxG.bgColor = {rgb: 0x131c1b, a: 0xff};
		#end
	}
}
