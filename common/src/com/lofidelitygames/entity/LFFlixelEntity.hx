package com.lofidelitygames.entity;

import org.flixel.FlxGroup;
import org.flixel.FlxObject;
import org.flixel.FlxSprite;

import com.lofidelitygames.data.LFSceneFile.EntityReference;
import com.lofidelitygames.data.LFEntityFile.EntityDescription;
import com.lofidelitygames.data.LFUtils;
import com.lofidelitygames.component.ILFComponent;
import com.lofidelitygames.component.LFFlixelComponent;

class LFFlixelEntity extends FlxObject, implements ILFEntity, implements ILFReceiver {

	public var sprite:FlxSprite;
	public var name:String;
	public var components(default, null):Hash<ILFComponent>;
	public var anim:ILFComponent;
	public var carriable:ILFComponent;

	public function init(description:EntityDescription, reference:EntityReference):Void {
		name = description.name;

		x = reference.position.x;
		y = reference.position.y;
		angle = reference.angle;

		width = (reference.width > 0) ? reference.width : description.bounding_box.width;
		height = (reference.height > 0) ? reference.height : description.bounding_box.height;

		velocity.x = description.velocity.x;
		velocity.y = description.velocity.y;

		acceleration.x = description.acceleration.x;
		acceleration.y = description.acceleration.y;

		drag.x = description.drag.x;
		drag.y = description.drag.y;

		maxVelocity.x = description.max_velocity.x;
		maxVelocity.y = description.max_velocity.y;

		if (description.sprite != null) {
			sprite = new FlxSprite(x, y);

			sprite.width = description.bounding_box.width;
			sprite.height = description.bounding_box.height;
			sprite.offset.x = description.bounding_box.offset.x;
			sprite.offset.y = description.bounding_box.offset.y;

			sprite.loadGraphic(
				description.sprite.path,
				description.sprite.animated,
				true,
				description.width,
				description.height
			);

			if (description.sprite.animated) {
				for (anim in description.sprite.sequences) {
					sprite.addAnimation(anim.name, anim.frames, anim.framerate, anim.looped);
				}

				sprite.play(description.sprite.default_sequence);
			}
		}

		components = new Hash<ILFComponent>();
		for (comp in description.components) {
			var comp_inst:LFFlixelComponent = LFUtils.instanceFor(comp.type, [this]);
			comp_inst.init(comp.parameters);
			components.set(comp.type, comp_inst);
		}

		exists = true;
	}

	public function receive(signal:String, message:Dynamic):Void {
		// trace(message);
	}

	override public function preUpdate():Void {
		if (sprite != null) {
			sprite.preUpdate();
		}
		super.preUpdate();
	}

	override public function update():Void {
		// let my components do their thang
		for (c in components) {
			c.update();
		}

		if (sprite != null) {
			sprite.update();
			sprite.x = x;
			sprite.y = y;
			sprite.angle = angle;
		}

		super.update();
	}

	override public function postUpdate():Void {
		if (sprite != null) {
			sprite.postUpdate();
		}
		super.postUpdate();
	}

	override public function draw():Void {
		if (sprite != null) {
			sprite.draw();
		}
		super.draw();
	}
}
