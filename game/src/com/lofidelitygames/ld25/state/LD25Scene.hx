package com.lofidelitygames.ld25.state;

import com.lofidelitygames.LFRegistry;
import org.flixel.FlxG;
import com.lofidelitygames.ld25.GameRegistry;
import com.lofidelitygames.state.LFFlixelScene;

class LD25Scene extends LFFlixelScene {

	override public function create():Void {
		super.create();
		GameRegistry.init();
		add(GameRegistry.ninjastars);
	}

	override public function update():Void {
		FlxG.collide(GameRegistry.ninjastars, collision_map);
		super.update();
	}

}
