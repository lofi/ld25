package com.lofidelitygames.data;

import haxe.xml.Fast;
import nme.Assets;
import nme.geom.Point;
import com.lofidelitygames.entity.ILFEntity;

typedef BoundingBoxDescription = {
	var width:Int;
	var height:Int;
	var offset:Point;
}

typedef AnimationDescription = {
	var name: String;
	var frames: Array<Int>;
	var framerate: Int;
	var looped: Bool;
}

typedef SpriteDescription = {
	var path: String;
	var animated: Bool;
	var sequences: Array<AnimationDescription>;
	var default_sequence: String;
}

typedef ComponentDescription = {
	var type: String;
	var parameters: Hash<String>;
}

typedef EntityDescription = {
	var name:String;
	var type:String;
	var health:Float;
	var width:Int;
	var height:Int;
	var bounding_box: BoundingBoxDescription;
	var velocity: Point;
	var acceleration: Point;
	var drag: Point;
	var max_velocity: Point;
	var sprite: SpriteDescription;
	var components: Array<ComponentDescription>;
}

class LFEntityFile {
	static public function load(path_to_asset:String):EntityDescription {
		var xml_text = Assets.getText(path_to_asset);
		var xml = new Fast( Xml.parse(xml_text).firstElement() );

		var bbox_description:BoundingBoxDescription = null;
		if (xml.hasNode.bounding_box) {
			bbox_description = {
				width : Std.parseInt(xml.node.bounding_box.node.width.innerData),
				height : Std.parseInt(xml.node.bounding_box.node.height.innerData),
				offset : new Point(
				  Std.parseInt(xml.node.bounding_box.node.offset.node.x.innerData),
					Std.parseInt(xml.node.bounding_box.node.offset.node.y.innerData)
				)
			};
		}

		var result:EntityDescription = {
			name : StringTools.trim(xml.node.name.innerData),
		  type : StringTools.trim(xml.node.type.innerData),
		  health : xml.hasNode.health ? Std.parseFloat(xml.node.health.innerData) : 100,
		  width : Std.parseInt(xml.node.width.innerData),
		  height : Std.parseInt(xml.node.height.innerData),
			bounding_box: bbox_description,
		  velocity : new Point(0, 0),
		  acceleration : new Point(0,0),
		  drag : new Point(0, 0),
		  max_velocity : new Point(10000, 10000),
			sprite : null,
			components : new Array<ComponentDescription>()
		};

		if (xml.hasNode.velocity) {
			var vel_node = xml.node.velocity;
			result.velocity.x = Std.parseInt(vel_node.node.x.innerData);
			result.velocity.y = Std.parseInt(vel_node.node.y.innerData);
		}

		if (xml.hasNode.acceleration) {
			var accel_node = xml.node.acceleration;
			result.acceleration.x = Std.parseInt(accel_node.node.x.innerData);
			result.acceleration.y = Std.parseInt(accel_node.node.y.innerData);
		}

		if (xml.hasNode.drag) {
			var drag_node = xml.node.drag;
			result.drag.x = Std.parseInt(drag_node.node.x.innerData);
			result.drag.y = Std.parseInt(drag_node.node.y.innerData);
		}

		if (xml.hasNode.max_velocity) {
			var max_vel_node = xml.node.max_velocity;
			result.max_velocity.x = Std.parseInt(max_vel_node.node.x.innerData);
			result.max_velocity.y = Std.parseInt(max_vel_node.node.y.innerData);
		}

		if (xml.hasNode.sprite) {
			var sprite_node = xml.node.sprite;
			var path = StringTools.trim(sprite_node.node.path.innerData);
			var animated = false;
			var default_sequence = null;
			var sequences = new Array<AnimationDescription>();

			if (sprite_node.hasNode.animation) {
				var anim_node = sprite_node.node.animation;

				default_sequence = StringTools.trim(anim_node.node.default_sequence.innerData);

				for (seq_node in anim_node.nodes.sequence) {
					var sequence_name = StringTools.trim(seq_node.node.name.innerData);
					var frames = Lambda.map(seq_node.node.frames.innerData.split(","), Std.parseInt);
					var framerate = Std.parseInt(seq_node.node.framerate.innerData);
					var looped = false;
					if (StringTools.trim(seq_node.node.looped.innerData) == "true") {
						looped = true;
					}

					var sequence = {
						name : sequence_name,
						frames : Lambda.array(frames),
						framerate : framerate,
						looped : looped
					};

					sequences.push(sequence);
				}

				animated = true;
			}

			result.sprite = {
				path : path,
				animated : animated,
				sequences : sequences,
				default_sequence : default_sequence
			};
		} // end of sprite settings

		for (comp_node in xml.nodes.component) {
			var type = StringTools.trim(comp_node.node.type.innerData);
			var parameters = new Hash<String>();
			result.components.push({type : type, parameters : parameters});
		}

		return result;
	}
}
