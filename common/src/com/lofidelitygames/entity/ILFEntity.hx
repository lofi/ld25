package com.lofidelitygames.entity;

import com.lofidelitygames.data.LFEntityFile.EntityDescription;
import com.lofidelitygames.data.LFSceneFile.EntityReference;
import com.lofidelitygames.component.ILFComponent;

interface ILFEntity {

	var components(default, null):Hash<ILFComponent>;

	function init(description:EntityDescription, reference:EntityReference):Void;
	function update():Void;

}
