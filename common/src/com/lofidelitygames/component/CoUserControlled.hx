package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.component.LFFlixelComponent;

// This component expects whatever entity it's attached to to have
// a sprite loaded.
class CoUserControlled extends LFFlixelComponent {

	private var my_jump_impulse_amount:Float = -500;

	override private function __update():Void {
		// always reset acceleration
		my_entity.acceleration.x = 0;

		if(!my_entity.alive) {
			// nothing to do when you're dead
			return;
		}

		if (FlxG.keys.LEFT) {
			my_entity.sprite.facing = FlxObject.LEFT;
			my_entity.acceleration.x -= my_entity.drag.x;
		} else if (FlxG.keys.RIGHT) {
			my_entity.sprite.facing = FlxObject.RIGHT;
			my_entity.acceleration.x += my_entity.drag.x;
		}

		if(FlxG.keys.justPressed("UP") && my_entity.velocity.y == 0) {
			my_entity.y -= 1;
			my_entity.velocity.y = my_jump_impulse_amount;

			my_entity.receive("sfx", "jump");
		}
	}

}
