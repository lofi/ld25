package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.component.LFFlixelComponent;

// This component
class CoWrapAtScreenEdge extends LFFlixelComponent {

	override private function __update():Void {
		my_entity.x = (my_entity.x + my_entity.width / 2 + FlxG.width) % FlxG.width - my_entity.width / 2;
		my_entity.y = (my_entity.y + my_entity.height / 2) % FlxG.height - my_entity.height / 2;
	}

}
