package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;
import org.flixel.FlxPoint;

import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.component.LFFlixelComponent;

// This component expects whatever entity it's attached to to have
// a sprite loaded.
class CoDumbass extends LFFlixelComponent {

	private var my_damage_amount:Float = 0.25;
	private var my_ambulation_amount:Int = 40;
	private var my_start_pos:FlxPoint;

	override public function init(parameters:Hash<String>):Void {
		my_start_pos = new FlxPoint(my_entity.x, my_entity.y);
		my_entity.sprite.facing = FlxObject.LEFT;
	}

	override private function __update():Void {
		my_entity.acceleration.x = 0;

		switch(my_entity.sprite.facing) {
			case FlxObject.LEFT:
				if (my_entity.x < my_start_pos.x - my_ambulation_amount) {
					my_entity.sprite.facing = FlxObject.RIGHT;
				} else {
					my_entity.acceleration.x -= my_entity.drag.x;
				};
			case FlxObject.RIGHT:
				if (my_entity.x > my_start_pos.x + my_ambulation_amount) {
					my_entity.sprite.facing = FlxObject.LEFT;
				} else {
					my_entity.acceleration.x += my_entity.drag.x;
				};
		}

		if (FlxG.overlap(my_entity, LFRegistry.player)) {
			LFRegistry.player.receive("damage", my_damage_amount);
		}
	}

}
