package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.component.LFFlixelComponent;
import com.lofidelitygames.entity.LFFlixelEntity;

// This is just being used to demonstrate some essential ideas
// it needs to be generalized of course and that will be done when
// the component parameters are implemented and read from the
// entity configuration.
class CoAnimateSprite extends LFFlixelComponent {

	override public function new(entity:LFFlixelEntity):Void {
		super(entity);
		entity.anim = this;
	}

	override private function __update():Void {
		var signal : String = 'play';
		var sequence : String;

		if (!my_entity.alive) {
			return;
		}

		if (my_entity.velocity.y != 0) {
			sequence = "jump";
		} else if (my_entity.velocity.x == 0) {
			sequence = "idle";
		} else {
			sequence = "walk";
		}
		my_entity.receive(signal, sequence);
	}

}
