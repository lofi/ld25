package com.lofidelitygames.ld25.component;

import org.flixel.FlxG;
import com.lofidelitygames.component.LFFlixelComponent;
import com.lofidelitygames.ld25.GameRegistry;

class CoVulnerableToNinjaStars extends LFFlixelComponent {

	override private function __update():Void {
		FlxG.overlap(my_entity, GameRegistry.ninjastars, function(a:Dynamic, b:Dynamic):Void {
			a.hurt(b.damage);
			b.kill();
		});
	}

}
