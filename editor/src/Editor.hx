package;

import nme.Lib;
import org.flixel.FlxG;
import org.flixel.FlxGame;

import editor.state.EditorMenuState;
import editor.state.SceneEditorState;

class Editor extends FlxGame
{
	private static var screen_factor:Float = 2.0;
	public function new()
	{
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;
		super(
			Math.floor(stageWidth / screen_factor),
			Math.floor(stageHeight / screen_factor),
			//EditorMenuState,
			SceneEditorState,
			screen_factor,
			60,
			30
		);
		forceDebugger = true;
	}
}
