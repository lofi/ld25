package com.lofidelitygames.entity;

interface ILFReceiver {
	public function receive(signal:String, message:Dynamic):Void;
}
