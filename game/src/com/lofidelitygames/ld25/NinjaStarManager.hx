package com.lofidelitygames.ld25;

import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.ld25.entity.NinjaStar;
import org.flixel.FlxGroup;
import org.flixel.FlxG;

class NinjaStarManager extends FlxGroup {

	public function new(pool_size:Int=10) {
		super();
		var i=0;
		while (i < pool_size) {
			var sprite = new NinjaStar();
			add(sprite);
			i++;
		}
	}

	public function fire(sx:Float, sy:Float, direction:Int):Void {
		var sprite = getFirstAvailable();
		if(sprite != null) {
			LFRegistry.sfx.get("throw").play();
			cast(sprite, NinjaStar).fire(sx, sy, direction);
		}
	}
}
