package com.lofidelitygames.ld25.entity;

import org.flixel.FlxG;
import org.flixel.FlxSound;
import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.entity.LFFlixelEntity;

class President extends LFFlixelEntity {

    override public function receive(signal:String, message:Dynamic):Void {
        switch(signal) {
            case "sfx":
                if (LFRegistry.sfx.exists(message)) {
                    LFRegistry.sfx.get(message).play();
                }
            case "play":
                switch(message){
                    default:
                        sprite.play(message);
                }
        }
    }

    override public function update():Void {
        if(this.overlaps(LFRegistry.player)){
            this.sprite.play('avoid');
        } else {
            this.sprite.play('idle');
        }
        super.update();
    }

}
