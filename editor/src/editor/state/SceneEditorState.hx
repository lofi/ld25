package editor.state;

import org.flixel.FlxTilemap;
import com.lofidelitygames.state.LFFlixelScene;
import nme.text.TextField;
import nme.text.TextFieldType;
import nme.Lib;

import org.flixel.FlxG;
import org.flixel.FlxObject;
import org.flixel.FlxTextField;
import org.flixel.FlxButton;

import com.lofidelitygames.data.LFSceneFile;

class SceneEditorState extends EditorState {
	private var TILE_WIDTH:Int;
	private var TILE_HEIGHT:Int;

	private var highlight_box:FlxObject;

	private var my_current_scene:SceneDescription;
	private var my_current_tilemap:FlxTilemap;

	public function new() {
		TILE_WIDTH = 16;
		TILE_HEIGHT = 16;

		my_current_scene = null;
		my_current_tilemap = null;

		super();
	}

	override public function create():Void {
		#if !neko
		FlxG.bgColor = 0xff006363;
		#else
		FlxG.bgColor = {rgb: 0x131c1b, a: 0xff};
		#end

		highlight_box = new FlxObject(0, 0, TILE_WIDTH, TILE_HEIGHT);

		my_current_scene = LFSceneFile.load("assets/scenes/test.xml");

		super.create();
	}

	override public function update():Void {
		// This provides a handy illustration of the tile currently under the mouse cursor
		highlight_box.x = Math.floor(FlxG.mouse.x / TILE_WIDTH) * TILE_WIDTH;
		highlight_box.y = Math.floor(FlxG.mouse.y / TILE_HEIGHT) * TILE_HEIGHT;

		super.update();
	}

	override public function draw():Void {
		// Draw our cursor
		highlight_box.drawDebug();

	 	my_current_tilemap.draw();

		super.draw();
	}

	override public function load():Void {
	}

	override public function quit():Void {
		Lib.exit();
	}

}
