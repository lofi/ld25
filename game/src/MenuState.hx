package;

import nme.Assets;
import nme.geom.Rectangle;
import nme.net.SharedObject;
import org.flixel.FlxButton;
import org.flixel.FlxG;
import org.flixel.FlxPath;
import org.flixel.FlxSave;
import org.flixel.FlxSprite;
import org.flixel.FlxState;
import org.flixel.FlxText;
import org.flixel.FlxU;

import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.ld25.GameRegistry;
import com.lofidelitygames.state.LFFlixelScene;
import com.lofidelitygames.ld25.state.LD25Scene;

class MenuState extends FlxState
{
	override public function create():Void
	{
		#if !neko
		FlxG.bgColor = 0xff131c1b;
		#else
		FlxG.bgColor = {rgb: 0x131c1b, a: 0xff};
		#end
		FlxG.mouse.show();

		var start_button = new FlxButton((FlxG.width/2)-20, FlxG.height/2, "START", function():Void {
			var description = LFRegistry.scene_descriptions.get("level01");
			FlxG.switchState(new LD25Scene(description));
		});
		add(start_button);
	}

	override public function destroy():Void
	{
		super.destroy();
	}

	override public function update():Void
	{
		super.update();
	}
}
