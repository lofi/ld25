package com.lofidelitygames.data;

import nme.Assets;
import haxe.xml.Fast;
import nme.geom.Point;

typedef TilemapDescription = {
	var name:String;
	var tiles:String;
	var tile_width:Int;
	var tile_height:Int;
	var map_width:Int;
	var map_height:Int;
	var data: Array<Int>;
}

typedef EntityReference = {
	var tag:String;
	var name:String;
	var position:Point;
	var angle:Int;
	var width:Int;
	var height:Int;
}

typedef SceneDescription = {
	var name:String;
	var mouse:Bool;
	var background_color:Int;
	var entities:Array<EntityReference>;
	var tilemaps:Array<TilemapDescription>;
}

class LFSceneFile {
	static public function load(path_to_asset:String):SceneDescription {

		var xml_text = Assets.getText(path_to_asset);
		var xml = new Fast( Xml.parse(xml_text).firstElement() );

		var tilemaps = new Array<TilemapDescription>();
		for(tilemap_node in xml.nodes.tilemap) {
			var tile_list = Lambda.map(tilemap_node.node.data.innerData.split(","), Std.parseInt);
			var tilemap_description:TilemapDescription = {
				name : StringTools.trim(tilemap_node.node.name.innerData),
			  tiles : StringTools.trim(tilemap_node.node.tiles.innerData),
			  tile_width : Std.parseInt(tilemap_node.node.tile_width.innerData),
			  tile_height : Std.parseInt(tilemap_node.node.tile_height.innerData),
		    map_width : Std.parseInt(tilemap_node.node.map_width.innerData),
		    map_height : Std.parseInt(tilemap_node.node.map_height.innerData),
		    data : Lambda.array(tile_list)
			};
			tilemaps.push(tilemap_description);
		}

		// entities are references to the items we place into the level
		// with information about where to put it and other simple arrangements
		var entity_data = new Array<EntityReference>();
		for (entity_node in xml.node.entities.nodes.entity) {

			var width = -1;
			if (entity_node.hasNode.width) {
				width = Std.parseInt(entity_node.node.width.innerData);
			}
			var height = -1;
			if (entity_node.hasNode.height) {
				height = Std.parseInt(entity_node.node.height.innerData);
			}

			var entity:EntityReference = {
				tag: StringTools.trim(entity_node.node.tag.innerData),
				name: StringTools.trim(entity_node.node.name.innerData),
				position: new Point(
				  Std.parseInt(entity_node.node.position.node.x.innerData),
					Std.parseInt(entity_node.node.position.node.y.innerData)
				),
				angle: entity_node.hasNode.angle ? Std.parseInt(entity_node.node.angle.innerData) : 0,
				width: width,
				height: height
			};

			entity_data.push(entity);
		}

		var mouse = true;
		if (xml.hasNode.mouse) {
			mouse = (StringTools.trim(xml.node.mouse.innerData) == "true");
		}

		return {
			name : StringTools.trim(xml.node.name.innerData),
		  mouse : mouse,
			background_color : Std.parseInt(xml.node.background_color.innerData),
		  tilemaps:tilemaps,
		  entities:entity_data
		};
	}

}
