package com.lofidelitygames.component;

import org.flixel.FlxG;
import org.flixel.FlxObject;

import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.state.LFFlixelScene;
import com.lofidelitygames.component.LFFlixelComponent;

// due to some bugs in HaxeFlixel this wont work as a generic component
// for entities. Probably better this way regardless because it just forces
// us to use FlxGroups instead which I think is a nice quadtree setup.
class CoCollideWithCurrentSceneTilemap extends LFFlixelComponent {

	override private function __update():Void {
		FlxG.collide(my_entity, LFRegistry.current_scene.collision_map);
	}

}
