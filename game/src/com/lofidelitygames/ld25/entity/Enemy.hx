package com.lofidelitygames.ld25.entity;

import org.flixel.FlxG;
import org.flixel.FlxSound;
import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.entity.LFFlixelEntity;

class Enemy extends LFFlixelEntity {

	private var my_invulnerability_duration:Float = 0.5;

	override public function receive(signal:String, message:Dynamic):Void {
		switch(signal) {
			case "damage":
				if (alive && !sprite.flickering) {
					sprite.flicker(my_invulnerability_duration);
					hurt(message);
					LFRegistry.sfx.get("hit").play();
				}
			case "sfx":
				if (LFRegistry.sfx.exists(message)) {
					LFRegistry.sfx.get(message).play();
				}
			case "play":
				sprite.play(message);
		}
	}
}
