package com.lofidelitygames.ld25.entity;

import com.lofidelitygames.ld25.component.CoNinjaStars;import org.flixel.FlxG;
import org.flixel.FlxSound;
import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.entity.LFFlixelEntity;
import com.lofidelitygames.ld25.component.CoNinjaStars;

class Player extends LFFlixelEntity {

	private var my_invulnerability_duration:Float = 1.5;

	override public function kill():Void {
		alive = false;
		exists = true;
		// trigger particle effect
		LFRegistry.sfx.get("splode").play();
		sprite.play('dead');
	}

	override public function hurt(damage:Float):Void {
		super.hurt(damage);
	}

	override public function receive(signal:String, message:Dynamic):Void {
		switch(signal) {
			case "damage":
				if (alive && !sprite.flickering) {
					sprite.flicker(my_invulnerability_duration);
					hurt(message);
					LFRegistry.sfx.get("hit").play();
				}
			case "sfx":
				if (LFRegistry.sfx.exists(message)) {
					LFRegistry.sfx.get(message).play();
				}
			case "play":
				sprite.play(message);
		}
	}

    private inline function get_ninja_stars():CoNinjaStars {
        // assumes there is only one instance of the ninja_stars component
        var ret:CoNinjaStars = null;
        for (c in components) {
            if (Std.is(c, CoNinjaStars)){
                ret = cast(c, CoNinjaStars);
            }
        }
        return ret;
    }

    override public function update():Void {
        if(LFRegistry.president.carriable.carried()){
            if(FlxG.keys.justReleased("Z")){
                LFRegistry.president.carriable.drop();
                get_ninja_stars().mute(false);
            }
        }
        else if (this.overlaps(LFRegistry.president)
            && !LFRegistry.president.carriable.carried()){

            if(FlxG.keys.justReleased("Z")){
                LFRegistry.president.carriable.pickup(this);
                get_ninja_stars().mute(true);
            }
        }
        super.update();
    }

}
