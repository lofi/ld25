package com.lofidelitygames.state;

import com.lofidelitygames.LFRegistry;
import org.flixel.FlxGroup;
import com.lofidelitygames.entity.LFFlixelEntity;
import nme.Lib;
import nme.Assets;
import nme.geom.Point;
import nme.geom.Rectangle;
import nme.net.SharedObject;
import org.flixel.FlxG;
import org.flixel.FlxPath;
import org.flixel.FlxSave;
import org.flixel.FlxObject;
import org.flixel.FlxSprite;
import org.flixel.FlxState;
import org.flixel.FlxText;
import org.flixel.FlxU;
import org.flixel.FlxTilemap;
import org.flixel.FlxAssets;
import addons.FlxCaveGenerator;

import com.lofidelitygames.LFRegistry;
import com.lofidelitygames.data.LFSceneFile;
import com.lofidelitygames.data.LFUtils;

class LFFlixelScene extends FlxState {

	private var my_scene_description:SceneDescription;

	public var collision_map(default, null):FlxTilemap;
	public var entities:FlxGroup;
	public var tagged_groups:Hash<FlxGroup>;

	public function new(scene:SceneDescription) {
		super();
		my_scene_description = scene;

		collision_map = null;
		entities = new FlxGroup();
		tagged_groups = new Hash<FlxGroup>();
	}

	override public function create():Void {
		super.create();

		FlxG.bgColor = my_scene_description.background_color;

		my_scene_description.mouse ? FlxG.mouse.show() : FlxG.mouse.hide();

		for (tilemap in my_scene_description.tilemaps) {
			createTilemap(tilemap);
		}

		for (entity in my_scene_description.entities) {
			createEntity(entity);
		}

		LFRegistry.current_scene = this;
	}

	private function createEntity(reference:EntityReference):Void {
		// level entity data only refers to a full description. So we have to
		// first load the entity description before it can be created

		var description = LFRegistry.entity_descriptions.get(reference.name);

		var entity:LFFlixelEntity = LFUtils.instanceFor(description.type, []);
		entity.init(description, reference);

		entities.add(entity);

		if (reference.tag == "player") {
			FlxG.camera.follow(entity);
			LFRegistry.player = entity;
		} else if (reference.tag == "president") {
			LFRegistry.president = entity;
		} else {
			if (!tagged_groups.exists(reference.tag)) {
				tagged_groups.set(reference.tag, new FlxGroup());
			}
			tagged_groups.get(reference.tag).add(entity);
		}

		add(entity);
	}

	private function createTilemap(tilemap_description:TilemapDescription):Void {
		var tilemap = new FlxTilemap();
		tilemap.widthInTiles = tilemap_description.map_width;
		tilemap.heightInTiles = tilemap_description.map_height;
		tilemap.loadMap(tilemap_description.data,
										tilemap_description.tiles,
										tilemap_description.tile_width,
										tilemap_description.tile_height,
										FlxTilemap.OFF);
		tilemap.updateFrameData();

		add(tilemap);

		if (tilemap_description.name == "collision") {
			collision_map = tilemap;
			collision_map.follow(FlxG.camera);
		}
	}

	public override function update():Void {
		collision_map.update();
		FlxG.collide(entities, collision_map);
		super.update();
	}

}
